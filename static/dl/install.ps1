#!/usr/bin/env pwsh
# Adapted from https://github.com/denoland/deno_install/blob/master/install.ps1
# Copyright 2019 Antipy V.O.F. All rights reserved. MIT license.
# TODO(everyone): Keep this script simple and easily auditable.

$ErrorActionPreference = 'Stop'

if ($args.Length -gt 0) {
  $Version = $args.Get(0)
} else {
  $Version = "modules"
}

if ($PSVersionTable.PSEdition -ne 'Core') {
  $IsWindows = $true
  $IsMacOS = $false
}

if ($args.Length -gt 1) {
  $BinDir = $args.Get(1)
} else {
  $BinDir = if ($IsWindows) {
    "$Home\.antibuild\bin"
  } else {
    "/usr/local/bin"
  }
}

$AntibuildExe = if ($IsWindows) {
  "$BinDir\antibuild.exe"
} else {
  "$BinDir/antibuild"
}

$OS = if ($IsWindows) {
  'windows'
} else {
  if ($IsMacOS) {
    'darwin'
  } else {
    'linux'
  }
}

$AntibuildUri = "https://gitlab.com/antipy/antibuild/cli/-/jobs/artifacts/${Version}/raw/dist/${OS}/amd64/antibuild?job=build"

if (!(Test-Path $BinDir)) {
  New-Item $BinDir -ItemType Directory | Out-Null
}

Invoke-WebRequest $AntibuildUri -OutFile $AntibuildExe -UseBasicParsing

if ($IsWindows) {
  $User = [EnvironmentVariableTarget]::User
  $Path = [Environment]::GetEnvironmentVariable('Path', $User)
  if (!(";$Path;".ToLower() -like "*;$BinDir;*".ToLower())) {
    [Environment]::SetEnvironmentVariable('Path', "$Path;$BinDir", $User)
    $Env:Path += ";$BinDir"
  }
  Write-Output "Antibuild was installed successfully to $AntibuildExe"
  Write-Output "Run 'antibuild --help' to get started"
} else {
  chmod +x "$AntibuildExe"
  Write-Output "Antibuild was installed successfully to $AntibuildExe"
  if (Get-Command antibuild -ErrorAction SilentlyContinue) {
    Write-Output "Run 'antibuild --help' to get started"
  } else {
    Write-Output "Manually add the directory to your `$HOME/.bash_profile (or similar)"
    Write-Output "  export PATH=`"${BinDir}:`$PATH`""
    Write-Output "Run '$AntibuildExe --help' to get started"
  }
}