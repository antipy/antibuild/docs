#!/bin/sh
# Adapted from https://github.com/denoland/deno_install/blob/master/install.sh
# Copyright 2019 Antipy V.O.F. All rights reserved. MIT license.
# TODO(everyone): Keep this script simple and easily auditable.

set -e

case $(uname -s) in
Darwin) os="darwin" ;;
*) os="linux" ;;
esac

case $(uname -m) in
x86_64) arch="amd64" ;;
*) arch="other" ;;
esac

if [ "$arch" = "other" ]; then
	echo "Unsupported architecture $(uname -m). Only x64 binaries are available."
	exit
fi

if [ $# -ge 1 ]; then
	antibuild_uri="https://gitlab.com/antipy/antibuild/cli/-/jobs/artifacts/${1}/raw/dist/${os}/${arch}/antibuild?job=build"
else
	antibuild_uri="https://gitlab.com/antipy/antibuild/cli/-/jobs/artifacts/modules/raw/dist/${os}/${arch}/antibuild?job=build"
fi

if [ $# -ge 2 ]; then
  bin_dir="${2}"
else
  bin_dir="/usr/local/bin"
fi

exe="$bin_dir/antibuild"

if [ ! -d "$bin_dir" ]; then
	mkdir -p "$bin_dir"
fi

curl -fL# -o "$exe" "$antibuild_uri"
chmod +x "$exe"

echo "Antibuild was installed successfully to $exe"
if command -v antibuild >/dev/null; then
  version=$(antibuild version)
	echo "Installed ${version}"
	echo "Run 'antibuild --help' to get started"
else
	echo "Manually add the directory to your \$HOME/.bash_profile (or similar)"
	echo "  export PATH=\"$bin_dir:\$PATH\""
	echo "Run '$exe --help' to get started"
fi
