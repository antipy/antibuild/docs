binary = ./antibuild
output_folder = public/

develop: clean
	$(binary) develop

build: clean
	$(binary) build

install_modules:
	$(binary) modules install

build_sass:
	yarn build

cloud_install:
	chmod +x ./static/dl/install.sh
	./static/dl/install.sh v1.0.0-pre2 .
	export binary=./$(binary)
	yarn

cloud_install_modules:
	#./$(binary) modules install

cloud_build: cloud_install clean cloud_install_modules build_sass
	./$(binary) build

clean:
	rm -rf $(output_folder)
