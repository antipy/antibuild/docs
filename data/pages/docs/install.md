---
last_updated: '9 september 2019'
---

# Installation

Antibuild can be installed in various ways. If you are just getting started you should probably install using the one click installer. If you are planning to contribute to the project you should build from source.

## One Click Installer (recommended)

### macOS and Linux

```bash
sudo curl https://build.antipy.com/dl/install.sh | sh
```

To install a specifc version pass a version number as the first argument after the `sh -s` like shown here:

```bash
sudo curl https://build.antipy.com/dl/install.sh | sh -s v1.0.0
```

By default this will install to the `/usr/local/bin` directory. To change this, set the second argument to the installation directory. Use `master` as version if you want the get the latest version. The command below would install into the current directory.

```bash
sudo curl https://build.antipy.com/dl/install.sh | sh -s master .
```

### Windows

```powershell
iwr https://build.antipy.com/dl/install.ps1 -useb | iex
```

To install a specifc version pass a version number as the first argument after the install script like shown here:

```powershell
iwr https://build.antipy.com/dl/install.ps1 -useb -outf install.ps1; .\install.ps1 v1.0.0
```

By default this will install to the `/usr/local/bin` directory. To change this, set the second argument to the installation directory. Use `master` as version (first argument) if you want the get the latest version. The command below would install into the `C:\Users\developer\Documents`.

```powershell
iwr https://build.antipy.com/dl/install.ps1 -useb -outf install.ps1; .\install.ps1 v1.0.0 C:\Users\developer\Documents
```

## Download binary

You can download a binary for your system [here](/downloads). Place this binary into a folder in your path, or into the directory you want to use Antibuild in.

## Installing from source

### Automatic

If you have the Go toolchain installed, run the command below to clone the repository into your gopath and to compile it to your gobin directory.

```bash
go install gitlab.com/antipy/antibuild/cli
```

### Manual

If you want to have more customization, clone the repository to your desired location using git like shown below:

```bash
git clone https://gitlab.com/antipy/antibuild/cli.git
```

Now enter the directory with cd and build a binary:

```bash
cd cli
go build -o antibuild cmd/*.go
```
