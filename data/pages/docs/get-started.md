---
last_updated: '9 september 2019'
---

# Getting Started

Antibuild is a tool used to build websites from templates, data and static files. It relies on templates for basic layout of your site, and can then fill these templates with data from data files or external sources such as Firebase or a REST API at compile time. You can also include static files for styling, images or anything else you want.

In this guide we will teach you how to set up Antibuild and how to get started with it.

## Installation

Antibuild can run completeley autonomously, however, if you want to use external modules that are not precompiled for your system you will need to install the Go toolchain. For detailed instructions please see the instructions for your OS below.

### macOS and Linux

Open your terminal and run the command below to install automatically.

```bash
sudo curl https://build.antipy.com/dl/install.sh | bash
```

### Windows

```bash
iwr https://build.antipy.com/dl/install.ps1 -useb | iex
```

For instructions on installing a specifc version or using a different install directory please view [this page](/install).

## Creating a new site

We will show you how Antibuild by creating a small blogging site. This guide assumes basic knowledge of the command prompt or terminal and of HTML, YAML and Markdown.

Open a command prompt or terminal and naviagte (`cd [base_directory]`) to the parent folder of where you want to start your project. You can now run `antibuild new`. You will be prompted to enter a name for your project. Let's call it `my-blog`.

You will now be asked what template you want to start with. Select the `basic` template with the arrow keys and press enter.

You can now configure what modules you want to pre install into the template. Lets choose `file` for opening files and `json` for decoding them, and then `markdown` so we have an easy way to write blog entries. You can always add more modules later. (Navigate with arrow keys, select with space and continue with enter)

Antibuild will now download the template and configure it with the selected modules. Once it is done you can navigate into the folder it created with `cd [project_name]`. To install the configured modules for your system enter `antibuild modules install`. Antibuild will now download all required modules from the standard repository.

You can now open the folder that was created in your favorite code editor or IDE. We recommend [vscode](https://code.visualstudio.com).

## Running Antibuild

Now that you have your code editor open, open a terminal and navigate to your project folder. Run `antibuild develop` to run compile your website and host it locally. You can now navigate to http://localhost:8080 in your browser to see the compiled page. As you can see there is currently not much to see. There is currently only a cover page.

There are four folders in your project directory

- _.modules_ holds all of your modules. This can be compared to a node_modules folder. You shouldn't touch this folder as it is generated automatically by the cli
- _data_ holds all of your data files. Data files can be stored in any folder on the system, but the convention is that you store your data files in the data folder.
- _templates_ holds all of your templates. These can be split up into many different files or some large files, and can be cross refrenced. You will learn more about this later.
- _static_ holds all of your static assets. This folder is copied over into the output directory before the html pages get compiled.

## Adding some data

Lets add some articles to our project. Create a new folder in the `data` folder called `articles`. Create a new markdown file (in this case `hello-world.md`) for your article in here. It will store the article contents and the article metadata. The top of the markdown file contains the article metadata in yaml (between the `---`). Underneath that there is the article content in CommonMark markdown:

```markdown
---
title: Hello World
author: luca
publish_date: 29/06/2019
---

This is the _hello world_ **article**.
```

We also need to give data about the authors. Lets add a `authors` folder in data directory. In there place a yaml file called `luca.yaml` and fill it with the data below. It stores metadata about our writers.

```yaml
name: Luca Casonato
loves:
  - JS/TS
  - Go
hates:
  - PHP
  - Python
```

## Adding templates

Lets start by adding the page layout shell. To do this create a file called `layout.html` in the `template` folder. The name for this file could be anything, but because it defines the layout we will call it `layout.html`. Start this file with `{{define "html"}}` and end it with `{{end}}`. This tells the template engine that this is the entrypoint for the page. We can add some layout wrapper code in between these tags:

```html
{{define "html"}}
<html>
  <head>
    <title>My Blog</title>
  </head>
  <body>
    <nav>
      <a href="/"> <h1>My Blog</h1></a>

      <a href="/articles">Articles</a> |
      <a href="/authors">Authors</a>
    </nav>
    {{template "page" .}}
  </body>
</html>
{{end}}
```

In here there is an embed to the `page` template. This page will dynamically change depending on the actual page we are building.

Now we will create the template for a single article - so the article view page. Create a file `article.html` file in the `template` folder. We will again define the template at the beginning and close it at the end:

```html
{{define "page"}}
<div>
  <!-- Parse the markdown into HTML -->
  {{markdown_common .body}}
</div>
{{end}}
```

Here we called our first module function. This function is called `common` (from the `markdown` module) and takes one argument. This argument is the raw markdown content from the data folder.

## Configuring Antibuild

We now need to configure Antibuild to actually build the articles. To do this we need to specify how and what data Antibuild should get and how to combine it with your templates. You can do this in the `config.json` file. This file contains a section called `pages`.

The top level element in this pages page should be our page `layout` wrapper. Import the layout template by adding `layout.html` to the `templates` array.

Next we will add an iterator to iterate over the `articles` we want to display pages for. Add an `iterators` object to `pages`. This object will map the name of the iterator to the function to run to get an iterable list from. This is called an iterator definition. The string we will use is `[file_pattern:data/articles/*.md]`. This is a function from the file module. It will output a list of the names of all files in the articles folder.

As a child of this layout page we will add our articles page. Add an array called `sites`. In here add a new object to specify your site. The slug (or url) of the site will be dynamic depending on the article name (from the iterator). Set the `slug` property in the sub site to `/articles/{{article}}.html`. At the place of `{{article}}` in the slug antibuild will insert an item from the iterator. Add a templates array with the article template in it (`article.html`).

Now we need to get the metadata and body data from the md file. Add a `data` array. In this array add `[file_file:data/articles/{{article}}.md][util_frontmatterbody:---]` and `[file_file:data/articles/{{article}}.md][yaml_frontmatter:---][util_prefix:meta]`. The first data item first opens the file, then removes the frontmatter in between the `---` and returns the remaining document under the `body` item. The second string opens a file, parses the yaml frontmatter between the `---` and returns the data under the `meta` key in the output data.

```json
{
...
"pages": {
  "iterators": {
    "article": "[file_pattern:data/articles/*.md]"
  },
  "templates": [
    "layout.html"
  ],
  "sites": [
    {
      "slug": "/articles/{{article}}.html",
      "templates": [
        "article.html"
      ],
      "data": [
        "data/articles/{{article}}.md"
      ]
    }
  ]
}
...
```
